<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes([
    'register' => false
]);

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/student/add', 'Admin\StudentController@addView');
Route::get('/student/all', 'Admin\StudentController@index');
Route::get('/student/{id}', 'Admin\StudentController@updateView');
Route::get('/student/delete/{id}', 'Admin\StudentController@delete');

Route::post('/student/add', 'Admin\StudentController@add')->name('student.add');
Route::post('/student/update', 'Admin\StudentController@update')->name('student.update');

Route::get('/subjects/course/{id}', 'Admin\CourseController@getSubjectsByCourse');
Route::get('/batch/{id}', 'Admin\BatchController@getByCourseId');
