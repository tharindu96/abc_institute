$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

function getBatch() {
    let id = document.getElementById('course').options[document.getElementById('course').selectedIndex].value;

    $.ajax({
        url: BASE + '/batch/' + id,
        method: "GET",
        async: true
    }).done(function (resp) {
        console.log(resp);
        document.getElementById("batch").disabled = false;
        var options = '';
        options += '<option selected>- Select -</option>';
        for (var i = 0, l = resp.length; i < l; i++) {
            var option = resp[i];
            options += '<option value="' + option['id'] + '">' + option['name'] + '</option>';
        }
        $("#batch").html(options);
    });
}

function getSubjects() {
    let id = document.getElementById('batch').options[document.getElementById('batch').selectedIndex].value;

    $.ajax({
        url: BASE + '/subjects/course/' + id,
        method: "GET",
        async: true
    }).done(function (resp) {
        console.log(resp['subjects'][0]['name']);
        var lis = '';
        for (var i = 0, l = resp['subjects'].length; i < l; i++) {
            var li = resp['subjects'][i];
            lis += `<li class="list-group-item">${li['name']}</li>`;
        }
        document.getElementById('rowSubjects').innerHTML = '';
        document.getElementById('rowSubjects').innerHTML = lis;
    });
}
