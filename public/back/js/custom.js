function navToggle(){
    if (document.getElementById("sideNavDiv").offsetWidth === 0) {
        document.getElementById("sideNavDiv").style.width = "300px";
        document.getElementById("contentDiv").style.marginLeft = "300px";
    }
    else if (document.getElementById("sideNavDiv").offsetWidth === 300) {
        document.getElementById("sideNavDiv").style.width = "0px";
        document.getElementById("contentDiv").style.marginLeft = "0px";
    }
}