<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ env('APP_NAME') }}</title>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
          integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('back/css/styles.css') }}">

    <script>
        var BASE = '{{url('/')}}';
    </script>
</head>
<body class="bg-light">
<div id="sideNavDiv">
    <div class="text-center pt-4 pb-4">
        <a href="{{url('/home')}}" class="btn mb-5"><h5 class="mb-0 text-light">ABC - ADMIN</h5></a>
        @guest
            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
            @if (Route::has('register'))
                <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
            @endif
        @else
            <p class="text-success font-weight-bold">Hi, {{ Auth::user()->name }}</p>
        @endguest
    </div>
    <div style="overflow: auto;">
        <div class="side-nav-link">
            <a href="{{url('/home')}}" class="btn btn-dark btn-block rounded-0 text-left p-2 pl-5 btn-main-link">
                <i class="fa fa-dashboard mr-3"></i> Dashboard
            </a>
        </div>
        <div class="side-nav-link">
            <button class="btn btn-dark btn-block rounded-0 text-left p-2 pl-5 btn-main-link" type="button"
                    data-toggle="collapse" data-target="#collapseStudents" aria-expanded="false"
                    aria-controls="collapseStudents">
                <i class="fa fa-user mr-3" style="width: 20px"></i>Students <i
                    class="fa fa-angle-down float-right"></i>
            </button>
            <div class="collapse" id="collapseStudents" style="border: 1px rgba(184, 184, 184, 0.1) solid;">
                <a href="{{url('/student/add')}}" class="btn btn-dark btn-block rounded-0 text-left"
                   style="padding-left: 80px;">
                    -> Add New Student
                </a>
                <a href="{{url('/student/all')}}" class="btn btn-dark btn-block rounded-0 text-left"
                   style="padding-left: 80px;">
                    -> All Students
                </a>
            </div>
        </div>

        <div class="side-nav-link">
            <a class="btn btn-dark btn-block rounded-0 text-left p-2 pl-5 btn-main-link" href="{{ route('logout') }}"
               onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                <i class="fa fa-arrow-left mr-3" style="width: 20px"></i>{{ __('Logout') }}
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST"
                  style="display: none;">
                @csrf
            </form>
        </div>
    </div>

</div>
<div id="contentDiv" class="bg-light">
    <div class="p-2" style="position: sticky; top: 0; background-color: #e3e6e8; z-index: 1;">
        <div class="row m-0">
            <button class="btn mr-2" onclick="navToggle()">
                <i class="fa fa-bars"></i>
            </button>
            @if(isset($breadcrumb))
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-0" style="background-color: #e3e6e8;">
                        @foreach($breadcrumb as $dat)
                            @if ($loop->last)
                                <li class="breadcrumb-item active" aria-current="page">{{ $dat['title'] }}</li>
                            @else
                                <li class="breadcrumb-item"><a href="{{ url($dat['link']) }}">{{ $dat['title'] }}</a>
                                </li>
                            @endif
                        @endforeach
                    </ol>
                </nav>
            @endif
        </div>
    </div>
    <div class="p-4">
        @yield('content')
    </div>
    <footer class="p-3 mt-5" style="background-color: #e3e6e8;">
        <small class="ml-4">Design & Developed by {{env('APP_DEVELOPER')}}</small>
    </footer>
</div>

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
        integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js"
        integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s"
        crossorigin="anonymous"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<script src="{{asset('back/js/custom.js')}}"></script>
@isset($styles)
    @if(count($styles))
        @foreach($styles as $value)
            <link href="{{asset('back/css/'.$value.'.css?v=1.'.rand(10,100))}}"/>
        @endforeach
    @endif
@endisset

@isset($scripts)
    @if(count($scripts))
        @foreach($scripts as $value)
            <script src="{{asset('back/js/'.$value.'.js?v=1.'.rand(10,100))}}"></script>
        @endforeach
    @endif
@endisset
@if(session('common_msg'))
    <script>
        swal("{{session('common_msg')['title']}}", "{{session('common_msg')['message']}}", "{{session('common_msg')['type']}}");
    </script>
@endif
</body>
</html>
