@extends('layouts.admin')
@section('content')
    <div class="card shadow">
        <div class="card-header">
            <h5 class="mb-0">{{$pg_title}}</h5>
        </div>
        <div class="card-body">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">First name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Contact</th>
                    <th scope="col" class="text-right">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($all_objs as $obj)
                    <tr>
                        <th scope="row">{{$obj->id}}</th>
                        <td>{{$obj->first_name}}</td>
                        <td>{{$obj->email}}</td>
                        <td>{{$obj->contact}}</td>
                        <td class="text-right">
                            <div class="dropdown">
                                <button class="btn btn-sm" type="button" id="{{'dropdownMenuButton'.$obj->id}}"
                                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fa fa-ellipsis-h"></i>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right"
                                     aria-labelledby="{{'dropdownMenuButton'.$obj->id}}">
                                    <a class="dropdown-item" href="{{url('/student/').'/'.$obj->id}}"><i
                                            class="fa fa-eye mr-3"></i>View</a>
                                </div>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <div class="row mt-5">
        <div class="col-sm-12 col-md-5">
            <div class="dataTables_info">Showing {{ $all_objs->firstItem() }}
                to {{ $all_objs->lastItem() }} of {{ $all_objs->total() }} entries
            </div>
        </div>
        <div class="col-sm-12 col-md-7">
            {{ $all_objs->links() }}
        </div>
    </div>
@endsection
