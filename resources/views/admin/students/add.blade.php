@extends('layouts.admin')
@section('content')
    <div class="card shadow">
        <div class="card-header">
            <h5 class="mb-0">{{$pg_title}}</h5>
        </div>
        <div class="card-body">
            <form class="text-left" method="POST" action="{{ route('student.add') }}">
                @csrf
                <span class="text-secondary small">Student info</span>
                <hr class="mt-0">
                <div class="form-row">
                    <div class="form-group col-md-3">
                        <label>Title</label>
                        <select name="title" class="form-control">
                            <option value="Mr">Mr.</option>
                            <option value="Miss">Miss.</option>
                            <option value="Mrs">Mrs.</option>
                            <option value="Honorable">Honorable.</option>
                        </select>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label>First name</label>
                        <input name="first_name" value="{{ old('first_name') }}" type="text"
                               class="form-control @error('first_name') is-invalid @enderror">
                        @error('first_name')
                        <span class="text-danger"><strong>{{ $message }}</strong></span>
                        @enderror
                    </div>
                    <div class="form-group col-md-6">
                        <label>Last name</label>
                        <input name="last_name" value="{{ old('last_name') }}" type="text"
                               class="form-control @error('last_name') is-invalid @enderror">
                        @error('last_name')
                        <span class="text-danger"><strong>{{ $message }}</strong></span>
                        @enderror
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label>Mobile</label>
                        <input name="mobile" type="number" value="{{ old('mobile') }}"
                               class="form-control @error('mobile') is-invalid @enderror">
                        @error('mobile')
                        <span class="text-danger"><strong>{{ $message }}</strong></span>
                        @enderror
                    </div>
                    <div class="form-group col-md-6">
                        <label>Email</label>
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                               name="email" value="{{ old('email') }}" required autocomplete="email">
                        @error('email')
                        <span class="text-danger"><strong>{{ $message }}</strong></span>
                        @enderror
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label>NIC</label>
                        <input name="nic" type="text" value="{{ old('nic') }}"
                               class="form-control @error('nic') is-invalid @enderror">
                        @error('nic')
                        <span class="text-danger"><strong>{{ $message }}</strong></span>
                        @enderror
                    </div>
                    <div class="form-group col-md-6">
                        <label>Date of birth</label>
                        <input id="dob" type="date" class="form-control @error('dob') is-invalid @enderror"
                               name="dob" value="{{ old('dob') }}" required>
                        @error('dob')
                        <span class="text-danger"><strong>{{ $message }}</strong></span>
                        @enderror
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-3">
                        <label>Gender</label>
                        <select name="gender" class="form-control">
                            <option value="Male">Male</option>
                            <option value="Female">Female</option>
                            <option value="Other">Other</option>
                        </select>
                    </div>
                </div>
                <span class="text-secondary small">Course info</span>
                <hr class="mt-0">
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label>Course</label>
                        <select id="course" name="course" class="form-control" required onchange="getBatch()">
                            <option selected>- Select -</option>
                            @if(isset($courses))
                                @foreach($courses as $obj)
                                    <option value="{{$obj->id}}">{{$obj->name}}</option>
                                @endforeach
                            @endif
                        </select>
                        @error('course')
                        <span class="text-danger"><strong>{{ $message }}</strong></span>
                        @enderror
                    </div>
                    <div class="form-group col-md-6">
                        <label>Batch</label>
                        <select id="batch" name="batch" class="form-control" required onchange="getSubjects()" disabled>
                        </select>
                        @error('batch')
                        <span class="text-danger"><strong>{{ $message }}</strong></span>
                        @enderror
                    </div>
                </div>
                <div class="form-row mt-4">
                    <div class="form-group col-md-6">
                        <span class="text-secondary small">Subjects</span>
                        <hr class="mt-0">
                        <ul class="list-group" id="rowSubjects">
                        </ul>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary">
                    Save
                </button>
            </form>
        </div>
    </div>
@endsection
