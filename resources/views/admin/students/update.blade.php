@extends('layouts.admin')
@section('content')
    <div class="card shadow">
        <div class="card-header">
            <h5 class="mb-0">{{$pg_title}}</h5>
        </div>
        <div class="card-body">
            <span class="text-secondary">Student info</span>
            <hr class="mt-0">
            <form class="text-left mb-3" method="POST" action="{{ route('student.update') }}">
                @csrf
                <input name="id" value="{{$obj->id}}" type="text" hidden>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label>First name</label>
                        <input name="first_name" value="{{ old('first_name',$obj->first_name) }}" type="text"
                               class="form-control @error('first_name') is-invalid @enderror">
                        @error('first_name')
                        <span class="text-danger"><strong>{{ $message }}</strong></span>
                        @enderror
                    </div>
                    <div class="form-group col-md-6">
                        <label>Last name</label>
                        <input name="last_name" value="{{ old('last_name',$obj->last_name) }}" type="text"
                               class="form-control @error('last_name') is-invalid @enderror">
                        @error('last_name')
                        <span class="text-danger"><strong>{{ $message }}</strong></span>
                        @enderror
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label>Mobile</label>
                        <input name="mobile" type="number" value="{{ old('mobile',$obj->contact) }}"
                               class="form-control @error('mobile') is-invalid @enderror">
                        @error('mobile')
                        <span class="text-danger"><strong>{{ $message }}</strong></span>
                        @enderror
                    </div>
                    <div class="form-group col-md-6">
                        <label>Email</label>
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                               name="email" value="{{ old('email',$obj->email) }}" required autocomplete="email">
                        @error('email')
                        <span class="text-danger"><strong>{{ $message }}</strong></span>
                        @enderror
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label>NIC</label>
                        <input name="nic" type="text" value="{{ old('nic',$obj->nic) }}"
                               class="form-control @error('nic') is-invalid @enderror">
                        @error('nic')
                        <span class="text-danger"><strong>{{ $message }}</strong></span>
                        @enderror
                    </div>
                </div>
                <button type="submit" class="btn btn-primary">
                    Update info
                </button>
            </form>
            <span class="text-secondary">Course info</span>
            <hr class="mt-0">
            <form class="text-left">
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label>Course</label>
                        <input type="text" class="form-control" value="{{ $course->name }}" disabled>
                    </div>
                    <div class="form-group col-md-6">
                        <label>Batch</label>
                        <input type="text" class="form-control" value="{{ $batch->name }}" disabled>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label>Subjects</label>
                        <ul class="list-group">
                            @if(isset($subjects))
                                @foreach($subjects as $subject)
                                    <li class="list-group-item">{{$subject->name}}</li>
                                @endforeach
                            @endif
                        </ul>
                    </div>
                </div>
            </form>

            <a href="{{url('/student/delete/'.$obj->id)}}" class="btn btn-danger mt-4">Delete student</a>
        </div>
    </div>
@endsection
