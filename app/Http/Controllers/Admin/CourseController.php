<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Batch;
use App\Models\Course;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class CourseController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getSubjectsByCourse(Request $request)
    {
        try {
            if ($request->ajax()) {
                $obj = Batch::find($request->id);
                $obj['subjects'] = $obj->course->subjects;
                return $obj;
            }
        } catch (\Exception $e) {
            Log::error($e);
        }
    }
}
