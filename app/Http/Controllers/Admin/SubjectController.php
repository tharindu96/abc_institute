<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class SubjectController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getByCourseId(Request $request)
    {
        try {
            if ($request->ajax()) {
                $obj = Batch::where('course_id', $request->id)->get();
                return $obj;
            }
        } catch (\Exception $e) {
            Log::error($e);
        }
    }
}
