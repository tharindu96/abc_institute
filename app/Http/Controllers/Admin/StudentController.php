<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Batch;
use App\Models\Course;
use App\Models\Student;
use App\Models\StudentBatch;
use App\Models\Subject;
use App\Traits\Messages;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class StudentController extends Controller
{
    use Messages;

    protected $resources = [
        'styles' => [],
        'scripts' => ['student']
    ];

    public function __construct()
    {
        $this->middleware('auth');
        $this->resources['courses'] = Course::all();
    }

    public function index()
    {
        try {
            $allArr = Student::select('*')->orderBy('id', 'desc')->paginate(10);
            $this->resources['all_objs'] = $allArr;

            $this->resources['pg_title'] = "All Students";
            return view('admin.students.all')->with($this->resources);
        } catch (\Exception $e) {
            dd($e->getMessage());
            Log::error($e);
        }
    }

    public function addView()
    {
        $this->resources['pg_title'] = "Add Student";
        return view('admin.students.add')->with($this->resources);
    }

    public function updateView(Request $request)
    {
        try {
            $student = Student::find($request->id);
            $this->resources['obj'] = $student;

            $student_batch_info = StudentBatch::where('student_id', $student->id)->first();
            $batch_info = Batch::find($student_batch_info->batch_id);
            $this->resources['batch'] = $batch_info;
            $this->resources['course'] = Course::find($batch_info->course_id);
            $this->resources['subjects'] = Subject::where('course_id', $batch_info->course_id)->get();

            $this->resources['pg_title'] = "Student View - (ID : " . $request->id . ")";
            return view('admin.students.update')->with($this->resources);
        } catch (\Exception $e) {
            dd($e->getMessage());
            Log::error($e);
        }
    }

    public function add(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'first_name' => ['required', 'string', 'max:100'],
                'last_name' => ['required', 'string', 'max:100'],
                'nic' => ['required', 'string'],
                'batch' => ['required'],
                'course' => ['required'],
                'mobile' => ['required', 'string', 'min:10', 'max:10'],
                'email' => ['required', 'string', 'email', 'max:255']
            ], $this->messages());

            if (!$validator->fails()) {
                DB::beginTransaction();
                $obj = new Student();
                $obj->title = $request->title;
                $obj->first_name = $request->first_name;
                $obj->last_name = $request->last_name;
                $obj->nic = $request->nic;
                $obj->date_of_birth = $request->dob;
                $obj->contact = $request->mobile;
                $obj->gender = $request->gender;
                $obj->email = $request->email;
                $obj->status = 1;
                $obj->save();

                $batch = new StudentBatch();
                $batch->student_id = $obj->id;
                $batch->batch_id = $request->batch;
                $batch->save();
                DB::commit();

                $this->resources['common_msg'] = $this->success;
                return redirect()->to('student/add')->with($this->resources);
            } else {
                return redirect()->back()->withErrors($validator)->withInput($request->all());
            }
        } catch (\Exception $e) {
            DB::rollBack();
            dd($e->getMessage());
            Log::error($e);
            $this->resources['common_msg'] = $this->danger;
            return redirect()->back()->with($this->resources);
        }
    }

    public function update(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'first_name' => ['required', 'string', 'max:100'],
                'last_name' => ['required', 'string', 'max:100'],
                'nic' => ['required', 'string'],
                'mobile' => ['required', 'string', 'min:10', 'max:10'],
                'email' => ['required', 'string', 'email', 'max:255']
            ], $this->messages());

            if (!$validator->fails()) {
                $obj = Student::find($request->id);
                $obj->first_name = $request->first_name;
                $obj->last_name = $request->last_name;
                $obj->nic = $request->nic;
                $obj->contact = $request->mobile;
                $obj->email = $request->email;
                $obj->save();

                $this->resources['common_msg'] = $this->success;
                return redirect()->to('student/' . $request->id)->with($this->resources);
            } else {
                return redirect()->back()->withErrors($validator)->withInput($request->all());
            }
        } catch (\Exception $e) {
            DB::rollBack();
            dd($e->getMessage());
            Log::error($e);
            $this->resources['common_msg'] = $this->danger;
            return redirect()->back()->with($this->resources);
        }
    }

    public function delete(Request $request)
    {
        try {
            Student::find($request->id)->delete();

            $this->resources['common_msg'] = $this->success;
            return redirect()->to('/student/all')->with($this->resources);
        } catch (\Exception $e) {
            dd($e->getMessage());
            Log::error($e);
            $this->resources['common_msg'] = $this->danger;
            return redirect()->to('/student/all')->with($this->resources);
        }
    }

    public function messages()
    {
        return [
            'first_name.required' => 'Please enter first name.',
            'last_name.required' => 'Please enter last name.',
            'first_name.max' => 'First name may not be greater than 200 characters.',
            'last_name.max' => 'Last name may not be greater than 200 characters.',
            'mobile.min' => 'Mobile number must be at least 10 digits.',
            'mobile.max' => 'Mobile number may not be greater than 10 digits.'
        ];
    }
}
