<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StudentBatch extends Model
{
    protected $table = 'student_batches';

    public $timestamps = false;
}
