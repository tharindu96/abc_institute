<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $table = 'courses';

    public $timestamps = false;

    public function subjects()
    {
        return $this->hasMany('App\Models\Subject', 'course_id');
    }
}
