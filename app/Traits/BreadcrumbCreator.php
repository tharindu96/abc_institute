<?php


namespace App\Traits;


trait BreadcrumbCreator
{

    public function setBreadcrumbVal($nameAndLinks){
        $dataArr = [];

        // Dashboard link always will be the 1st link
        array_push($dataArr, ['title' => 'Dashboard', 'link' => 'admin/home']);

        foreach ($nameAndLinks as $data){
            array_push($dataArr, ['title' => ucfirst($data[0]), 'link' => $data[1]]);
        }

        return $dataArr;
    }

    public function setBreadcrumbValDashboard(){
        $dataArr = [];
        array_push($dataArr, ['title' => 'Dashboard', 'link' => 'admin/home']);
        return $dataArr;
    }

    public function setBreadcrumbValFront($nameAndLinks){
        $dataArr = [];

        // Dashboard link always will be the 1st link
        array_push($dataArr, ['title' => 'Home', 'link' => 'home']);

        foreach ($nameAndLinks as $data){
            array_push($dataArr, ['title' => ucfirst($data[0]), 'link' => $data[1]]);
        }

        return $dataArr;
    }
}
